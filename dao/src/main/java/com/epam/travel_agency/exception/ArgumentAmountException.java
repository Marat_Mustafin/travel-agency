/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.exception;

/**
 *
 * @author Marat_Mustafin
 */
public class ArgumentAmountException extends Exception {

    private final static String MESAGE = "Incorrect arguments amount. ";

    public ArgumentAmountException(int correctAmount, int currentAmount) {
	super(MESAGE + "(Allowed " + correctAmount + " argument(s), but found " + correctAmount + " argument(s)");
    }

    public ArgumentAmountException() {
    }

    public ArgumentAmountException(String message) {
	super(MESAGE + message);
    }

    public ArgumentAmountException(String message, Throwable cause) {
	super(MESAGE + message, cause);
    }

    public ArgumentAmountException(Throwable cause) {
	super(cause);
    }

}
