package com.epam.travel_agency.config;

import com.epam.travel_agency.dao.TourDao;
import com.epam.travel_agency.dao.specification.tour.DiapasonDurationListSpecification;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.util.Properties;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@PropertySource("classpath:datasource.properties")
@ComponentScan(basePackages = "com.epam.travel_agency", excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = DaoConfig.class)})
@EnableTransactionManagement
public class DaoConfig {

    private static final String COM_EPAM_TRAVEL_AGENCY_DOMAIN = "com.epam.travel_agency.domain";
    private static final String HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String ATRUE = "true";
    private static final String HIBERNATEGLOBALLY_QUOTED_IDENTIFIERS = "hibernate.globally_quoted_identifiers";
    private static final String HIBERNATE_CONNECTION_AUTO_RECONNECT_FOR_POOLS = "hibernate.connection.autoReconnectForPools";
    private static final String HIBERNATE_CONNECTION_AUTO_RECONNECT = "hibernate.connection.autoReconnect";
    private static final String HIBERNATE_USE_SQL_COMMENTS = "hibernate.use_sql_comments";
    private static final String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    private static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String URL_CONNECTION = "jdbc:postgresql://127.0.0.1:5432/travel_agency";
    private static final String POSTGRES = "postgres";
    private static final String DRIVER_FQN = "org.postgresql.Driver";

    private final Logger LOGGER = LoggerFactory.getLogger(DaoConfig.class);

    @Value(URL_CONNECTION)
    private String jdbcUrl;
    @Value(POSTGRES)
    private String user;
    @Value(POSTGRES)
    private String password;
    @Value(DRIVER_FQN)
    private String className;

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
	HikariConfig config = new HikariConfig();
	config.setJdbcUrl(jdbcUrl);
	config.setUsername(user);
	config.setPassword(password);
	config.setDriverClassName(className);
	return new HikariDataSource(config);
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
	LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	sessionFactory.setDataSource(dataSource());
	sessionFactory.setPackagesToScan(COM_EPAM_TRAVEL_AGENCY_DOMAIN);
	sessionFactory.setHibernateProperties(hibernateProperties());
	return sessionFactory;
    }

    @Bean
    public PlatformTransactionManager hibernateTransactionManager() {
	HibernateTransactionManager transactionManager = new HibernateTransactionManager();
	transactionManager.setSessionFactory(sessionFactory().getObject());
	return transactionManager;
    }

    private final Properties hibernateProperties() {
	return new Properties() {
	    {
		setProperty(HIBERNATEGLOBALLY_QUOTED_IDENTIFIERS, ATRUE);
		setProperty(HIBERNATE_DIALECT, environment.getProperty(HIBERNATE_DIALECT));
		setProperty(HIBERNATE_SHOW_SQL, environment.getProperty(HIBERNATE_SHOW_SQL));
		setProperty(HIBERNATE_FORMAT_SQL, environment.getProperty(HIBERNATE_FORMAT_SQL));
		setProperty(HIBERNATE_USE_SQL_COMMENTS, environment.getProperty(HIBERNATE_USE_SQL_COMMENTS));
		setProperty(HIBERNATE_CONNECTION_AUTO_RECONNECT, environment.getProperty(HIBERNATE_CONNECTION_AUTO_RECONNECT));
		setProperty(HIBERNATE_CONNECTION_AUTO_RECONNECT_FOR_POOLS, environment.getProperty(HIBERNATE_CONNECTION_AUTO_RECONNECT_FOR_POOLS));
	    }

	};
    }

}
