/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.tour;

import com.epam.travel_agency.dao.specification.Specification;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.NoArgsConstructor;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marat_Mustafin
 */
@Component
@NoArgsConstructor
public class DiapasonDateListSpecification extends Specification<LocalDate> {

    private static final String SELECT_MI_NDATE_MIN_MA_XDATE_MAX_FROM_TOURS = "SELECT MIN(date) min, MAX(date) max FROM tours";

    @Override
    public List<LocalDate> search() {
	NativeQuery<Object[]> nativeQuery = sessionFactory.getCurrentSession().createSQLQuery(SELECT_MI_NDATE_MIN_MA_XDATE_MAX_FROM_TOURS);
	Object[] result = nativeQuery.getSingleResult();
	List<LocalDate> resultLocalDate = new ArrayList<>(2);
	resultLocalDate.add(convertToLocalDate((Date) result[0]));
	resultLocalDate.add(convertToLocalDate((Date) result[1]));
	return resultLocalDate;
    }

    private LocalDate convertToLocalDate(Date dateToConvert) {
	return Instant.ofEpochMilli(dateToConvert.getTime())
		.atZone(ZoneId.systemDefault())
		.toLocalDate();
    }
}
