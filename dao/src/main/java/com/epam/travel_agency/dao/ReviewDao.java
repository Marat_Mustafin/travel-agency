/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.Review;
import com.epam.travel_agency.domain.Tour;
import com.epam.travel_agency.domain.User;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marat_Mustafin
 */
@Repository
@Transactional
public class ReviewDao extends AbstractDao<Review> {

    private static final String FROM_REVIEW = "FROM Review";

    @Override
    public Optional<Review> findById(int id) {
	LOGGER.info("Read data from DB (review by id) " + id);
	return Optional.ofNullable(sessionFactory.getCurrentSession().get(Review.class, id));
    }

    @Override
    public List<Review> findAll() {
	return super.findAll(FROM_REVIEW);
    }
}
