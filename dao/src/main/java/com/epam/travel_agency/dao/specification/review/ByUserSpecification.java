/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.review;

import com.epam.travel_agency.dao.specification.Specification;
import com.epam.travel_agency.domain.Review;
import com.epam.travel_agency.domain.User;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.hibernate.Session;

/**
 *
 * @author Marat_Mustafin
 */
@AllArgsConstructor
public class ByUserSpecification extends Specification<Review> {

    private static final String USER = "user";
    private static final String ID_USER = "id_user";

    private User user;

    @Override
    public List<Review> search() {
	Session session = sessionFactory.getCurrentSession();
	CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
	CriteriaQuery<Review> criteriaQuery = criteriaBuilder.createQuery(Review.class);
	Root<Review> rootEntry = criteriaQuery.from(Review.class);
	ParameterExpression<User> parameterUser = criteriaBuilder.parameter(User.class, ID_USER);
	criteriaQuery.select(rootEntry).where(criteriaBuilder.equal(rootEntry.get(USER), parameterUser));
	return session.createQuery(criteriaQuery).setParameter(ID_USER, user).list();
    }

}
