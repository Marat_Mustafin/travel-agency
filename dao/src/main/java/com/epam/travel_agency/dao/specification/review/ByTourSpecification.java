/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.review;

import com.epam.travel_agency.dao.specification.Specification;
import com.epam.travel_agency.domain.Review;
import com.epam.travel_agency.domain.Tour;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.hibernate.Session;

/**
 *
 *
 * @author Marat_Mustafin
 */
@AllArgsConstructor
public class ByTourSpecification extends Specification<Review> {

    private static final String TOUR = "tour";
    private static final String ID_TOUR = "id_tour";

    private Tour tour;

    @Override
    public List<Review> search() {
	Session session = sessionFactory.getCurrentSession();
	CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
	CriteriaQuery<Review> criteriaQuery = criteriaBuilder.createQuery(Review.class);
	Root<Review> rootEntry = criteriaQuery.from(Review.class);
	ParameterExpression<Tour> parameterTour = criteriaBuilder.parameter(Tour.class, ID_TOUR);
	criteriaQuery.select(rootEntry).where(criteriaBuilder.equal(rootEntry.get(TOUR), parameterTour));
	return session.createQuery(criteriaQuery).setParameter(ID_TOUR, tour).list();
    }

}
