/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.tour;

import com.epam.travel_agency.dao.specification.Specification;
import java.util.ArrayList;
import java.util.List;
import lombok.NoArgsConstructor;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marat_Mustafin
 */
@Component
@NoArgsConstructor
public class DiapasonDurationListSpecification extends Specification<Integer> {

    private static final String SELECT_MI_NDURATION_MIN_MA_XDURATION_MAX_FR = "SELECT MIN(duration) min, MAX(duration) max FROM tours";

    @Override
    public List<Integer> search() {
	NativeQuery<Object[]> nativeQuery = sessionFactory.getCurrentSession().createSQLQuery(SELECT_MI_NDURATION_MIN_MA_XDURATION_MAX_FR);
	Object[] result = nativeQuery.getSingleResult();
	List<Integer> resultInteger = new ArrayList<>(2);
	resultInteger.add((int) result[0]);
	resultInteger.add((int) result[1]);
	return resultInteger;
    }

}
