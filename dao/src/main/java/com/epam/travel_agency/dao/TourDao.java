/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.Tour;
import java.util.List;
import java.util.Optional;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marat_Mustafin
 */
@Repository
@Transactional
public class TourDao extends AbstractDao<Tour> {

    private static final String FROM_TOUR = "FROM Tour";

    @Override
    public Optional<Tour> findById(int id) {
	LOGGER.info("Read data from DB (tour by id) " + id);
	Optional<Tour> tourOptional = Optional.ofNullable(sessionFactory.getCurrentSession().get(Tour.class, id));
	if (tourOptional.isPresent()) {
	    Hibernate.initialize(tourOptional.get().getReview());
	}
	return tourOptional;
    }

    @Override
    public List<Tour> findAll() {
	return super.findAll(FROM_TOUR);
    }

}
