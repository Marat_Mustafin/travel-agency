/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.tour;

import com.epam.travel_agency.dao.specification.Specification;
import com.epam.travel_agency.domain.Tour;
import com.epam.travel_agency.domain.User;
import java.util.List;
import lombok.Setter;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marat_Mustafin
 */
@Setter
@Component
public class AllToursByUserSpecification extends Specification<Tour> {

    private static final String SELECT_FROM_TOURS_JOIN_USER_TOURS_ON_ID = "SELECT * FROM tours  JOIN user_tours ON id =tour_id WHERE user_id = ";
    
    private User user;

    @Override
    public List<Tour> search() {
	NativeQuery<Tour> nativeQuery = sessionFactory.getCurrentSession().createSQLQuery(SELECT_FROM_TOURS_JOIN_USER_TOURS_ON_ID + user.getId());
	nativeQuery.addEntity(Tour.class);
	return nativeQuery.list();
    }

}
