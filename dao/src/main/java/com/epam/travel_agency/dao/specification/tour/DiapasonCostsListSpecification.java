/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.tour;

import com.epam.travel_agency.dao.specification.Specification;
import java.util.ArrayList;
import java.util.List;
import lombok.NoArgsConstructor;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marat_Mustafin
 */
@Component
@NoArgsConstructor
public class DiapasonCostsListSpecification extends Specification<Float> {

    private static final String SELECT_MI_NCOST_MIN_MA_XCOST_MAX_FROM_TOURS = "SELECT MIN(cost) min, MAX(cost) max FROM tours";
    
    @Override
    public List<Float> search() {
	NativeQuery<Object[]> nativeQuery = sessionFactory.getCurrentSession().createSQLQuery(SELECT_MI_NCOST_MIN_MA_XCOST_MAX_FROM_TOURS);
	Object[] result = nativeQuery.getSingleResult();
	List<Float> resultDouble = new ArrayList<>(2);
	resultDouble.add((float) result[0]);
	resultDouble.add((float) result[1]);
	return resultDouble;
    }

}
