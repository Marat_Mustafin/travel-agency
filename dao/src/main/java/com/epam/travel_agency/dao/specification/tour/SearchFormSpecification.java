/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.tour;

import com.epam.travel_agency.dao.specification.Specification;
import com.epam.travel_agency.domain.Country;
import com.epam.travel_agency.domain.Tour;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marat_Mustafin
 */
@NoArgsConstructor
@Setter
@Component
public class SearchFormSpecification extends Specification<Tour> {

    private static final String HOTEL = "hotel";
    private static final String STARS = "stars";
    private static final String COST = "cost";
    private static final String TOUR_TYPE = "tourType";
    private static final String DURATION = "duration";
    private static final String DATE = "date";
    private static final String NAME = "name";
    private static final String COUNTRY = "country";
    private static final String ID = "id";

    private Country country;
    private LocalDate date[];
    private int duration[];
    private Tour.TourType tourType;
    private float cost[];
    private int stars;
    private int limit;
    private int id;
    private Direction direction;

    @Override
    public List<Tour> search() {
	CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
	CriteriaQuery<Tour> criteriaQuery = criteriaBuilder.createQuery(Tour.class);
	Root<Tour> rootEntry = criteriaQuery.from(Tour.class);
	Predicate predicateId = (direction == Direction.PREVIOUS) ? criteriaBuilder.lessThan(rootEntry.get(ID), id) : criteriaBuilder.greaterThan(rootEntry.get(ID), id);
	Predicate predicateCountry = criteriaBuilder.equal(rootEntry.get(COUNTRY).get(NAME), country.getName());
	Predicate predicateDate = criteriaBuilder.between(rootEntry.get(DATE), date[0], date[1]);
	Predicate predicateDuration = criteriaBuilder.between(rootEntry.get(DURATION), duration[0], duration[1]);
	Predicate predicateTourType = criteriaBuilder.equal(rootEntry.get(TOUR_TYPE), tourType);
	Predicate predicateCost = criteriaBuilder.between(rootEntry.get(COST), cost[0], cost[1]);
	Predicate predicateStars = criteriaBuilder.equal(rootEntry.get(HOTEL).get(STARS), stars);
	criteriaQuery.select(rootEntry).where(predicateId, predicateCountry, predicateDate, predicateDuration, predicateTourType, predicateCost, predicateStars);
	return sessionFactory.getCurrentSession().createQuery(criteriaQuery).setMaxResults(limit).list().stream().sorted(Comparator.comparing(Tour::getId)).collect(Collectors.toList());
    }

    public enum Direction {
	NEXT, PREVIOUS, NONE
    }

}
