/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao;

import com.epam.travel_agency.dao.specification.Specification;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marat_Mustafin
 * @param <T>
 */
@Transactional
@Repository
public abstract class AbstractDao<T> {

    protected static Logger LOGGER = LoggerFactory.getLogger(AbstractDao.class);

    @Autowired
    protected SessionFactory sessionFactory;

    public List<T> findBySpecification(Specification<T> speicification) {
	return speicification.search();
    }
    
    public Object findObjectBySpecification(Specification<? extends Object> speicification) {
	return speicification.search();
    }

    public void add(T subject) {
	LOGGER.info("Create data in DB " + subject);
	sessionFactory.getCurrentSession().save(subject);
    }

    public void update(T subject) {
	LOGGER.info("Update data in DB " + subject);
	sessionFactory.getCurrentSession().update(subject);
    }

    public void remove(T subject) {
	LOGGER.info("Remove data from DB " + subject);
	sessionFactory.getCurrentSession().delete(subject);
    }

    public Optional<T> findById(int id) {
	throw new UnsupportedOperationException();
    }

    public List<T> findAll() {
	throw new UnsupportedOperationException();
    }

    List<T> findAll(String currentEntity) {
	LOGGER.info("Read data from DB " + currentEntity);
	return sessionFactory.getCurrentSession().createQuery(currentEntity).list();
    }
}
