/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.Hotel;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marat_Mustafin
 */
@Repository
@Transactional
public class HotelDao extends AbstractDao<Hotel> {

    private static final String FROM_HOTEL = "FROM Hotel";

    @Override
    public Optional<Hotel> findById(int id) {
	LOGGER.info("Read data to DB (hotel by id) " + id);
	return Optional.ofNullable(sessionFactory.getCurrentSession().get(Hotel.class, id));
    }

    @Override
    public List<Hotel> findAll() {
	return super.findAll(FROM_HOTEL);

    }
}
