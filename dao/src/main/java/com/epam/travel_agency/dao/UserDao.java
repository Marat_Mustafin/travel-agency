/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.User;
import java.util.List;
import java.util.Optional;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marat_Mustafin
 */
@Repository
@Transactional
public class UserDao extends AbstractDao<User> {

    private static final String FROM_USER = "FROM User";
    
    @Override
    public Optional<User> findById(int id) {
	LOGGER.info("Read data from DB (user by id) " + id);
	return Optional.ofNullable(sessionFactory.getCurrentSession().get(User.class, id));
    }

    @Override
    public List<User> findAll() {
	return super.findAll(FROM_USER);
    }

}
