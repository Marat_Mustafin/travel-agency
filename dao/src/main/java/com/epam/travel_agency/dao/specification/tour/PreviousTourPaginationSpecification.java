/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.tour;

import com.epam.travel_agency.dao.specification.Specification;
import com.epam.travel_agency.domain.Tour;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marat_Mustafin
 */
@Component
@NoArgsConstructor
@Setter
public class PreviousTourPaginationSpecification extends Specification<Tour> {

    private static final String ORDER_BY_ID_DESC_LIMIT = " ORDER BY id DESC LIMIT ";
    private static final String SELECT_FROM_TOURS_WHERE_ID_LESS = "SELECT * FROM tours WHERE id < ";

    private int id;
    private int limit;

    @Override
    public List<Tour> search() {
	NativeQuery<Tour> nativeQuery = sessionFactory.getCurrentSession().createSQLQuery(SELECT_FROM_TOURS_WHERE_ID_LESS + id + ORDER_BY_ID_DESC_LIMIT + limit);
	nativeQuery.addEntity(Tour.class);
	return nativeQuery.list();
    }
}
