/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.tour;

import com.epam.travel_agency.dao.specification.Specification;
import com.epam.travel_agency.domain.Tour;
import java.util.List;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marat_Mustafin
 */
@Component
@NoArgsConstructor
@Setter
public class NextTourPaginationSpecification extends Specification<Tour> {

    private static final String ORDER_BY_ID_ASC_LIMIT = " ORDER BY id ASC LIMIT ";
    private static final String SELECT__FROM_TOURS_WHERE_ID_MORE = "SELECT * FROM tours WHERE id > ";

    private int id;
    private int limit;

    @Override
    public List<Tour> search() {
	NativeQuery<Tour> nativeQuery = sessionFactory.getCurrentSession().createSQLQuery(SELECT__FROM_TOURS_WHERE_ID_MORE + id + ORDER_BY_ID_ASC_LIMIT + limit);
	nativeQuery.addEntity(Tour.class);
	return nativeQuery.list();
    }
}
