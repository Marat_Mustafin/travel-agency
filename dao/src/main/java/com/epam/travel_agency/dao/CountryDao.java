/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.Country;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marat_Mustafin
 */
@Repository
@Transactional
public class CountryDao extends AbstractDao<Country> {

    private static final String FROM_COUNTRY = "FROM Country";

    @Override
    public Optional<Country> findById(int id) {
	LOGGER.info("Read data from DB (country by id) " + id);
	return Optional.ofNullable(sessionFactory.getCurrentSession().get(Country.class, id));
    }

    @Override
    public List<Country> findAll() {
	return super.findAll(FROM_COUNTRY);
    }

}
