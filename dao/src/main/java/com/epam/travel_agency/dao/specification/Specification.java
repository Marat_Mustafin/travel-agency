/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marat_Mustafin
 */
@Component
public abstract class Specification<T> {

    @Autowired
    protected SessionFactory sessionFactory;

    public abstract List<T> search();
}
