/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.maptype;

import com.epam.travel_agency.domain.HotelFeatures;
import java.io.Serializable;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.EnumSet;
import java.util.Objects;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

/**
 *
 * @author Marat_Mustafin
 */
public class HotelFeaturesMap implements UserType {

    private static final String FEATURES = "features";

    @Override
    public int[] sqlTypes() {
	return new int[]{Types.JAVA_OBJECT};
    }

    @Override
    public Class returnedClass() {
	return EnumSet.class;
    }

    @Override
    public boolean equals(Object x, Object y) {
	return Objects.equals(x, y);
    }

    @Override
    public int hashCode(Object x) {
	return x.hashCode();
    }

    @Override
    public Object deepCopy(Object value) {
	return value;
    }

    @Override
    public boolean isMutable() {
	return false;
    }

    @Override
    public Serializable disassemble(Object o) {
	return (Serializable) o;
    }

    @Override
    public Object assemble(
	    Serializable cached,
	    Object owner) {
	return cached;
    }

    @Override
    public Object replace(
	    Object o,
	    Object target,
	    Object owner) {
	return o;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] strings, SharedSessionContractImplementor ssci, Object o) throws HibernateException, SQLException {
	EnumSet<HotelFeatures> features = EnumSet.noneOf(HotelFeatures.class);
	Array featuresSqlArray = (Array) rs.getArray(strings[0]);
	if (!rs.wasNull()) {
	    Object[] objects = (Object[]) featuresSqlArray.getArray();
	    for (Object feature : objects) {
		features.add(HotelFeatures.valueOf(((String) feature).toUpperCase()));
	    }
	}
	return features;
    }

    @Override
    public void nullSafeSet(PreparedStatement ps, Object object, int i, SharedSessionContractImplementor ssci) throws HibernateException, SQLException {
	if (null == object) {
	    ps.setNull(i, Types.JAVA_OBJECT);
	} else {
	    EnumSet<HotelFeatures> enumSetFeatures = (EnumSet<HotelFeatures>) object;
	    HotelFeatures featuresArray[] = (HotelFeatures[]) enumSetFeatures.stream().toArray(HotelFeatures[]::new);
	    Array sqlArray = ssci.connection().createArrayOf(FEATURES, featuresArray);
	    ps.setArray(i, sqlArray);
	}
    }

}
