/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao.specification.user;

import com.epam.travel_agency.dao.specification.Specification;
import com.epam.travel_agency.domain.User;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

/**
 *
 * @author Marat_Mustafin
 */
@Setter
@Service
public class ByLoginSpecification extends Specification<User> {

    private static final String USERNAME = "username";

    private String login;
    private boolean loadReview = false;

    @Override
    public List<User> search() {
	Session session = sessionFactory.getCurrentSession();
	CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
	CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
	Root<User> rootEntry = criteriaQuery.from(User.class);
	criteriaQuery.select(rootEntry).where(criteriaBuilder.equal(rootEntry.get(USERNAME), login));
	User user = session.createQuery(criteriaQuery).uniqueResult();
	if (loadReview) {
	    if (null != user) {
		Hibernate.initialize(user.getReview());
	    }
	}
	return Arrays.asList(user);
    }

}
