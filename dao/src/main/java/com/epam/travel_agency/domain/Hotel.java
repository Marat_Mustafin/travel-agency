package com.epam.travel_agency.domain;

import com.epam.travel_agency.dao.maptype.HotelFeaturesMap;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "hotels")
@Getter
@Setter
@ToString
@EqualsAndHashCode(exclude = {"tour"})
@NoArgsConstructor
@AllArgsConstructor
@Component
@TypeDef(
	name = "features",
	typeClass = HotelFeaturesMap.class
)
public class Hotel {

    private static final String ID = "id";
    private static final String HOTEL = "hotel";
    private static final String NAME = "name";
    private static final String STARS = "stars";
    private static final String WEBSITE = "website";
    private static final String LALITUDE = "lalitude";
    private static final String LALITUDE_MIN = "0.0";
    private static final String LONGITUDE = "longitude";
    private static final String LONGITUDE_MIN = "0.0";
    private static final String FEATURES = "features";

    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(mappedBy = HOTEL, fetch = FetchType.LAZY)
    private List<Tour> tour = new ArrayList<>();

    @Column(name = NAME)
    @NotNull
    @Length(min = 3, max = 255)
    private String name;

    @Column(name = STARS)
    @NotNull
    @Min(value = 1)
    @Max(value = 5)
    private int stars;

    @Column(name = WEBSITE)
    @NotNull
    @Length(min = 6, max = 255)
    private String webSite;

    @Column(name = LALITUDE)
    @NotNull
    @DecimalMin(LALITUDE_MIN)
    private double lalitude;

    @Column(name = LONGITUDE)
    @NotNull
    @DecimalMin(LONGITUDE_MIN)
    private double longitude;

    @Type(type = FEATURES)
    @Column(name = FEATURES, nullable = false)
    @NotNull
    private EnumSet<HotelFeatures> features = EnumSet.noneOf(HotelFeatures.class);

    public Hotel(String name, int stars, String webSite, double lalitude, double longitude,
	    EnumSet<HotelFeatures> features) {
	this.name = name;
	this.stars = stars;
	this.webSite = webSite;
	this.lalitude = lalitude;
	this.longitude = longitude;
	this.features = features;
    }

}
