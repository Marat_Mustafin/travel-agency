package com.epam.travel_agency.domain;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "reviews")
@Getter
@Setter
@ToString
@EqualsAndHashCode()
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Review {

    private static final String ID = "id";
    private static final String DATE = "date";
    private static final String TEXT = "text";
    private static final String ID_USER = "id_user";
    private static final String ID_TOUR = "id_tour";
    
    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = DATE)
    @NotNull
    private LocalDate date;

    @Column(name = TEXT)
    @NotNull
    @Length(min = 3, max = 255)
    private String text;

    @NotNull
    @ManyToOne
    @JoinColumn(name = ID_USER)
    private User user;

    @NotNull
    @ManyToOne
    @JoinColumn(name = ID_TOUR)
    private Tour tour;

    public Review(LocalDate date, String text, User user, Tour tour) {
	super();
	this.date = date;
	this.text = text;
	this.user = user;
	this.tour = tour;
    }
}
