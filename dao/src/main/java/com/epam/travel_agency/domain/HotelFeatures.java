/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.domain;

/**
 *
 * @author Marat_Mustafin
 */
public enum HotelFeatures {
    WIFI, POOL, PARKING, GYM, BUFFET, CINEMA, WHOLE_FLOOR_RENT, OCEAN_VIEW, SPA, BAR;
}
