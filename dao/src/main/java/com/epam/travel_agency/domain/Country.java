package com.epam.travel_agency.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "countries")
@Getter
@Setter
@ToString
@EqualsAndHashCode(exclude = {"tour"})
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Country {

    private static final String ID = "ID";
    private static final String COUNTRY = "country";
    private static final String NAME = "name";

    @Id
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(mappedBy = COUNTRY, fetch = FetchType.EAGER)
    private List<Tour> tour = new ArrayList<>();

    @Column(name = NAME)
    @NotNull
    @Length(min = 3, max = 255)
    private String name;

    public Country(String name) {
	this.name = name;
    }

}
