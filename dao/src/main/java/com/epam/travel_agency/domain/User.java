package com.epam.travel_agency.domain;

import com.epam.travel_agency.dao.maptype.UserRoleMap;
import com.epam.travel_agency.userrole.UserRoleEnum;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "users")
@Getter
@Setter
@Builder
@ToString(exclude = {"review"})
@EqualsAndHashCode(exclude = {"review"})
@NoArgsConstructor
@AllArgsConstructor
@Component
@SuppressWarnings("UniqueEntityName")
@TypeDef(
	name = "user_role",
	typeClass = UserRoleMap.class
)
public class User {

    private static final String ID = "id";
    private static final String USER = "user";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String USER_ROLE = "user_role";
    private static final String ROLE = "role";

    @Id
    @NotNull
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(mappedBy = USER, fetch = FetchType.LAZY)
    private List<Review> review = new ArrayList<>();

    @Column(name = LOGIN)
    @NotBlank
    @NotNull
    @Length(min = 3, max = 255)
    private String username;

    @Column(name = PASSWORD)
    @NotBlank
    @NotNull
    @Length(min = 3, max = 255)
    private String password;

    @Type(type = USER_ROLE)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = ROLE)
    private UserRoleEnum userRole;

    public User(String login, String password) {
	this.username = login;
	this.password = password;
    }

}
