package com.epam.travel_agency.domain;

import com.epam.travel_agency.dao.maptype.TourTypeMap;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "tours")
@Getter
@Setter
@EqualsAndHashCode(exclude = {"reviews"})
@NoArgsConstructor
@AllArgsConstructor
@Component
@TypeDef(
	name = "tour_type",
	typeClass = TourTypeMap.class
)
public class Tour {

    public static enum TourType {
	BUISNESS, CRUIS, TRIP, HONEYMOON, EXCURSION, ADVENTURE, SHOPPING, FAMILY, HEALTH, CHARITY
    }

    private static final String ID = "id";
    private static final String ID_HOTEL = "id_hotel";
    private static final String ID_COUNTRY = "id_country";
    private static final String TOUR = "tour";
    private static final String PHOTO = "photo";
    private static final String DATE = "date";
    private static final String DURATION = "duration";
    private static final String DESCRIPRION = "description";
    private static final String COST = "cost";
    private static final String COST_MIN = "0.0";
    private static final String TOUR_TYPE = "tour_type";

    @Id
    @NotNull
    @Column(name = ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = ID_HOTEL)
    private Hotel hotel;

    @NotNull
    @ManyToOne
    @JoinColumn(name = ID_COUNTRY)
    private Country country;

    @OneToMany(mappedBy = TOUR, fetch = FetchType.LAZY)
    private List<Review> review = new ArrayList<>();

    @Column(name = PHOTO)
    @NotNull
    @Length(min = 4, max = 255)
    private String photoName;

    @Column(name = DATE)
    @NotNull
    private LocalDate date;

    @Column(name = DURATION)
    @NotNull
    @Min(value = 1)
    private int duration;

    @Column(name = DESCRIPRION)
    @NotNull
    @Length(min = 3, max = 255)
    private String description;

    @Column(name = COST)
    @NotNull
    @DecimalMin(COST_MIN)
    private double cost;

    @Type(type = TOUR_TYPE)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = TOUR_TYPE)
    private TourType tourType;

    public Tour(String photoName, LocalDate date, int duration, String description, double cost,
	    TourType tourType, Hotel hotel, Country country) {
	super();
	this.photoName = photoName;
	this.date = date;
	this.duration = duration;
	this.description = description;
	this.cost = cost;
	this.tourType = tourType;
	this.hotel = hotel;
	this.country = country;
    }

}
