/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.userrole;

import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Marat_Mustafin
 */
public enum UserRoleEnum {

    ADMIN,
    MEMBER,
    GUEST;

    UserRoleEnum() {
    }

}
