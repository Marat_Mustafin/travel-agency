--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

CREATE TYPE public.features AS ENUM (
    'GYM',
    'WIFI',
    'POOL',
    'WHOLE_FLOOR_RENT',
    'PARKING',
    'BUFFET',
    'SPA',
    'OCEAN_VIEW',
    'BAR',
    'CINEMA'
);
ALTER TYPE public.features OWNER TO postgres;

CREATE TYPE public.tour_type AS ENUM (
    'BUISNESS',
    'CRUIS',
    'TRIP',
    'HONEYMOON',
    'EXCURSION',
    'ADVENTURE',
    'SHOPING',
    'FAMILILY',
    'HEALTH',
    'CHARITY'
);
ALTER TYPE public.tour_type OWNER TO postgres;

SET default_tablespace = '';
SET default_with_oids = false;

CREATE TABLE public.countries (
    id integer NOT NULL,
    name text NOT NULL
);
ALTER TABLE public.countries OWNER TO postgres;

CREATE SEQUENCE public.countries_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.countries_id_seq OWNER TO postgres;
ALTER SEQUENCE public.countries_id_seq OWNED BY public.countries.id;

CREATE TABLE public.hotels (
    id integer NOT NULL,
    name text NOT NULL,
    stars integer NOT NULL,
    website text NOT NULL,
    lalitude real NOT NULL,
    longitude real NOT NULL,
    features public.features[]
);
ALTER TABLE public.hotels OWNER TO postgres;

CREATE SEQUENCE public.hotel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.hotel_id_seq OWNER TO postgres;
ALTER SEQUENCE public.hotel_id_seq OWNED BY public.hotels.id;

CREATE TABLE public.reviews (
    id integer NOT NULL,
    id_user integer NOT NULL,
    date date NOT NULL,
    text text NOT NULL,
    id_tour integer NOT NULL
);
ALTER TABLE public.reviews OWNER TO postgres;

CREATE SEQUENCE public.review_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.review_id_seq OWNER TO postgres;
ALTER SEQUENCE public.review_id_seq OWNED BY public.reviews.id;

CREATE TABLE public.tours (
    id integer NOT NULL,
    date date NOT NULL,
    duration integer NOT NULL,
    description text NOT NULL,
    cost real NOT NULL,
    id_hotel integer NOT NULL,
    id_country integer NOT NULL,
    photo text,
    tour_type public.tour_type NOT NULL
);
ALTER TABLE public.tours OWNER TO postgres;

CREATE SEQUENCE public.tours_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.tours_id_seq OWNER TO postgres;
ALTER SEQUENCE public.tours_id_seq OWNED BY public.tours.id;

CREATE TABLE public.users (
    id integer NOT NULL,
    login text NOT NULL,
    password text NOT NULL
);
ALTER TABLE public.users OWNER TO postgres;

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.users_id_seq OWNER TO postgres;

CREATE TABLE public.user_tours
(
    user_id integer NOT NULL,
    tour_id integer NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public.user_tours OWNER to postgres;

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
ALTER TABLE ONLY public.countries ALTER COLUMN id SET DEFAULT nextval('public.countries_id_seq'::regclass);
ALTER TABLE ONLY public.hotels ALTER COLUMN id SET DEFAULT nextval('public.hotel_id_seq'::regclass);
ALTER TABLE ONLY public.reviews ALTER COLUMN id SET DEFAULT nextval('public.review_id_seq'::regclass);
ALTER TABLE ONLY public.tours ALTER COLUMN id SET DEFAULT nextval('public.tours_id_seq'::regclass);
ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.hotels
    ADD CONSTRAINT hotel_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT review_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.tours
    ADD CONSTRAINT tours_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_id_tour_fkey FOREIGN KEY (id_tour) REFERENCES public.tours(id);

ALTER TABLE ONLY public.tours
    ADD CONSTRAINT tours_id_country_fkey FOREIGN KEY (id_country) REFERENCES public.countries(id);

ALTER TABLE ONLY public.tours
    ADD CONSTRAINT tours_id_hotel_fkey FOREIGN KEY (id_hotel) REFERENCES public.hotels(id);

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT users_reviews_id_user_fkey FOREIGN KEY (id_user) REFERENCES public.users(id);
