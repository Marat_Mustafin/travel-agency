package com.epam.travel_agency.config;

import com.epam.travel_agency.config.DaoConfig;
import junit.framework.TestCase;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = "com.epam.travelagency", excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = AppTestConfig.class),
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = DaoConfig.class)})
@Import({DaoConfig.class})
public class AppTestConfig extends TestCase {
    
}
