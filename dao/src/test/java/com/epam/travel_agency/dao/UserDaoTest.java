/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.User;
import com.epam.travel_agency.config.AppTestConfig;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Marat_Mustafin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(classes = AppTestConfig.class)
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    private int generatedId;
    private final static int ID = 1;
    private final static String LOGIN = "T-E-S-T login";
    private final static String PASSWORRD = "T-E-S-T password";
    private final static User expectedUser = new User();

    @BeforeClass
    public static void setDefaultUserParameters() {
	expectedUser.setId(ID);
//	expectedUser.setLogin(LOGIN);
	expectedUser.setPassword(PASSWORRD);
    }

    @Test
    @Ignore
    public void addTest() {
	userDao.add(expectedUser);
	User actualReview = userDao.findById(expectedUser.getId()).get();
	Assert.assertEquals(expectedUser, actualReview);
    }

    @Test
    @Ignore
    public void updateTest() {
//	expectedUser.setLogin(LOGIN + LOGIN);
	expectedUser.setPassword(PASSWORRD + PASSWORRD);
	userDao.update(expectedUser);
	User actualUser = userDao.findById(expectedUser.getId()).get();
	Assert.assertEquals(expectedUser, actualUser);
    }

    @Test
    @Ignore
    public void removeTest() {
	userDao.remove(expectedUser);
	Assert.assertNull(userDao.findById(generatedId));
    }
}
