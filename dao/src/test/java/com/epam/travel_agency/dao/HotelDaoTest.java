package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.Hotel;
import com.epam.travel_agency.domain.HotelFeatures;
import com.epam.travel_agency.config.AppTestConfig;
import java.util.EnumSet;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(classes = AppTestConfig.class)
public class HotelDaoTest {

    @Autowired
    @Qualifier("hotelDao")
    private HotelDao hotelDao;

    private final static String NAME = "T-E-S-T";
    private final static String WEBSITE = "www.T-E-S-T.by";
    private final static int STARS = 5;
    private final static double LALITUDE = 0.0;
    private final static double LONGITUDE = 0.0;
    private final static Hotel expectedHotel = new Hotel();

    @BeforeClass
    public static void setDefaultHotelParameters() {
	EnumSet<HotelFeatures> features = EnumSet.noneOf(HotelFeatures.class);
	features.add(HotelFeatures.GYM);
	expectedHotel.setFeatures(features);
	expectedHotel.setName(NAME);
	expectedHotel.setStars(STARS);
	expectedHotel.setWebSite(WEBSITE);
	expectedHotel.setLalitude(LALITUDE);
	expectedHotel.setLongitude(LONGITUDE);
    }

    @Test
    @Ignore
    public void addTest() {
	hotelDao.add(expectedHotel);
	Hotel actualHotel = hotelDao.findById(expectedHotel.getId()).get();
	Assert.assertEquals(expectedHotel, actualHotel);
    }

    @Test
    @Ignore
    public void updateTest() {
	expectedHotel.setId(1);
	EnumSet<HotelFeatures> features = EnumSet.noneOf(HotelFeatures.class);
	features.add(HotelFeatures.BUFFET);
	expectedHotel.setFeatures(features);
	expectedHotel.setName(NAME + NAME);
	expectedHotel.setStars(STARS - 1);
	expectedHotel.setWebSite(WEBSITE + WEBSITE);
	expectedHotel.setLalitude(LALITUDE + 1);
	expectedHotel.setLongitude(LONGITUDE + 1);
	hotelDao.update(expectedHotel);
	Hotel actualHotel = hotelDao.findById(expectedHotel.getId()).get();
	Assert.assertEquals(expectedHotel, actualHotel);
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    @Ignore
    public void removeTest() {
	expectedHotel.setId(1);
	hotelDao.remove(expectedHotel);
	Assert.assertNull(hotelDao.findById(expectedHotel.getId()));
    }
}
