package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.Review;
import com.epam.travel_agency.domain.Tour;
import com.epam.travel_agency.domain.User;
import com.epam.travel_agency.config.AppTestConfig;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(classes = AppTestConfig.class)
public class ReviewDaoTest {

    @Autowired
    @Qualifier("reviewDao")
    private ReviewDao reviewDao;

    @Autowired
    private UserDao userDao;
    @Autowired
    private TourDao tourDao;

    private final static Review expectedReview = new Review();

    @BeforeClass
    public static void setDefaultReviewParameters() {
	expectedReview.setId(4);
	expectedReview.setText("T-E-S-T");
	expectedReview.setDate(LocalDate.of(2018, 12, 31));
    }

    @Test
    @Ignore
    public void addTest() {
	expectedReview.setUser(userDao.findById(1).get());
	expectedReview.setTour(tourDao.findById(1).get());
	reviewDao.add(expectedReview);
	Review actualReview = reviewDao.findById(expectedReview.getId()).get();
	Assert.assertEquals(expectedReview, actualReview);
    }

    @Test
    @Ignore
    public void updateTest() {
	expectedReview.setUser(userDao.findById(2).get());
	expectedReview.setTour(tourDao.findById(2).get());
	reviewDao.update(expectedReview);
	Review actualReview = reviewDao.findById(expectedReview.getId()).get();
	Assert.assertEquals(expectedReview, actualReview);
    }

    @Test
    @Ignore
    public void removeTest() {
	reviewDao.remove(expectedReview);
	Assert.assertNull(reviewDao.findById(expectedReview.getId()));
    }

    @Test
    @Ignore
    public void findTourByUserTest() {
	List<Review> expectedTours = new ArrayList<>();
	User user = userDao.findById(13).get();
	user.setId(13);

	Review review = new Review();
	review.setId(21);
	review.setTour(tourDao.findById(731).get());
	review.setUser(user);
	review.setDate(LocalDate.of(2017, 6, 6));
	review.setText("Oeisk aog x tp ukvk. Ltwpv gwt w hf ojhd. Kpphj yvu n qa iixk.");
	expectedTours.add(review);

	review = new Review();
	review.setId(894);
	review.setTour(tourDao.findById(553).get());
	review.setUser(user);
	review.setDate(LocalDate.of(2016, 3, 14));
	review.setText("Jhrrf nld z yg iqxu. Ncuij eip v ag uhal. Vsbhx oeq k dq bajf.");
	expectedTours.add(review);

//	List<Review> actualTours = reviewDao.findReviewByUser(user);
//	Assert.assertEquals(expectedTours, actualTours);
    }

    @Test
    @Ignore
    public void findReviewByTourTest() {
	List<Review> expectedTours = new ArrayList<>();
	Tour tour = tourDao.findById(16).get();

	Review review = new Review();
	review.setId(141);
	review.setTour(tour);
	review.setUser(userDao.findById(49).get());
	review.setDate(LocalDate.of(2017, 11, 25));
	review.setText("Ndqbf rrv u qc avet. Vlqnd vol g jk zbxu. Iuxgq xmw d ig fsuj.");
	expectedTours.add(review);

	review = new Review();
	review.setId(537);
	review.setTour(tour);
	review.setUser(userDao.findById(97).get());
	review.setDate(LocalDate.of(2016, 12, 3));
	review.setText("Uiqxt znn d fl jona. Mibzo pyj r de wngy. Rbkel nac j ko zcjd.");
	expectedTours.add(review);

//	List<Review> actualTours = reviewDao.findReviewByTour(tour);
//	Assert.assertEquals(expectedTours, actualTours);
    }
}
