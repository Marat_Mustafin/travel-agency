/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.Country;
import com.epam.travel_agency.domain.Tour;
import com.epam.travel_agency.domain.Tour.TourType;
import com.epam.travel_agency.config.AppTestConfig;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Marat_Mustafin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppTestConfig.class)
public class TourDaoTest {

    private static ApplicationContext context = new AnnotationConfigApplicationContext(AppTestConfig.class);

    private static UserDao userDao = context.getBean(UserDao.class);
    private static TourDao tourDao = context.getBean(TourDao.class);
    private static HotelDao hotelDao = context.getBean(HotelDao.class);
    private static CountryDao countryDao = context.getBean(CountryDao.class);

    private final static Tour expectedTour = new Tour();

    @BeforeClass
    public static void setDefaultTourParameters() {
	expectedTour.setPhotoName("T-E-S-T photo");
	expectedTour.setDescription("T-E-S-T description");
	expectedTour.setDate(LocalDate.of(2018, 12, 12));
	expectedTour.setDuration(10);
	expectedTour.setCost(1.5);
	expectedTour.setTourType(TourType.CRUIS);
	expectedTour.setHotel(hotelDao.findById(1).get());
	expectedTour.setCountry(countryDao.findById(1).get());
    }

    @Test
    @Ignore
    public void addTest() {
	tourDao.add(expectedTour);
	Tour actualTour = tourDao.findById(expectedTour.getId()).get();
	Assert.assertEquals(expectedTour, actualTour);
    }

    @Test
    @Ignore
    public void updateTest() {
	expectedTour.setId(1);
	expectedTour.setCountry(countryDao.findById(2).get());
	expectedTour.setHotel(hotelDao.findById(2).get());
	tourDao.update(expectedTour);
	Tour actualTour = tourDao.findById(expectedTour.getId()).get();
	Assert.assertEquals(expectedTour, actualTour);
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    @Ignore
    public void removeTest() {
	expectedTour.setId(1);
	tourDao.remove(expectedTour);
	Assert.assertNull(tourDao.findById(expectedTour.getId()));
    }

    @Test
    @Ignore
    public void findAllToursByUserSqlTest() {
	List<Tour> expectedTouts = new ArrayList<>();
	Tour tour = new Tour();
	tour.setId(1);
	tour.setDate(LocalDate.of(2018, Month.DECEMBER, 12));
	tour.setDuration(10);
	tour.setDescription("T-E-S-T description");
	tour.setCost(1.5);
	tour.setHotel(hotelDao.findById(2).get());
	tour.setCountry(countryDao.findById(2).get());
	tour.setPhotoName("T-E-S-T photo");
	tour.setTourType(TourType.CRUIS);
	expectedTouts.add(tour);
//	Assert.assertEquals(expectedTouts, tourDao.findAllToursByUserSql(userDao.findById(1).get()));
    }

    @Test
    @Ignore
    public void findTourByCostTest() {
	List<Tour> expectedTours = new ArrayList<>();
	Tour tour = new Tour();
	tour.setId(158);
	tour.setDate(LocalDate.of(2018, Month.JANUARY, 7));
	tour.setDuration(12);
	tour.setDescription("Ppgiu nyz bt wodr. Ugeft und zg tdxb. Itlrt fap be hxro.");
	tour.setCost(777);
	tour.setHotel(hotelDao.findById(92).get());
	tour.setCountry(countryDao.findById(17).get());
	tour.setPhotoName("vwpo.png");
	tour.setTourType(TourType.CRUIS);
	expectedTours.add(tour);

	tour = new Tour();
	tour.setId(789);
	tour.setDate(LocalDate.of(2016, Month.JUNE, 18));
	tour.setDuration(12);
	tour.setDescription("Yvgms ddf td cyjd. Mzmyy nef tz uree. Jmbnc xhk wa sdct.");
	tour.setCost(777);
	tour.setHotel(hotelDao.findById(44).get());
	tour.setCountry(countryDao.findById(12).get());
	tour.setPhotoName("eswe.png");
	tour.setTourType(TourType.CRUIS);
	expectedTours.add(tour);

//	Assert.assertEquals(expectedTours, tourDao.findTourByCost(777));
    }

    @Test
    @Ignore
    public void findTourByHotelStarsTest() {//Already particular checked, that DB contains 186 tours with one-star hotels
//	Assert.assertEquals(186, tourDao.findTourByHotelStars(1).size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    @Ignore
    public void findTourByParticularCriteriesTest() {
	Country country = countryDao.findById(1).get();
//	List<Tour> actualTour = tourDao.findTourByParticularCriteries(country, LocalDate.of(2017, Month.AUGUST, 12), 5, TourType.CRUIS, 1700, 5);
//	Assert.assertNull(actualTour.get(1));
    }
}
