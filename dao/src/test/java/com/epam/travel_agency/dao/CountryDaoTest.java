/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dao;

import com.epam.travel_agency.domain.Country;
import com.epam.travel_agency.config.AppTestConfig;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Marat_Mustafin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Component
@ContextConfiguration(classes = AppTestConfig.class)
public class CountryDaoTest {

    @Autowired
    @Qualifier("countryDao")
    private CountryDao countryDao;

    private final static Country expectedContry = new Country();

    @BeforeClass
    public static void setDefaultCountryParameters() {
	expectedContry.setId(1);
	expectedContry.setName("T-E-S-T country name");
    }

    @Test
    @Ignore
    public void addTest() {
	countryDao.add(expectedContry);
	Country actualCountry = countryDao.findById(expectedContry.getId()).get();
	Assert.assertEquals(expectedContry, actualCountry);
    }

    @Test
    @Ignore
    public void updateTest() {
	expectedContry.setName("T-E-S-T another name");
	countryDao.update(expectedContry);
	Country actualCountry = countryDao.findById(expectedContry.getId()).get();
	Assert.assertEquals(expectedContry, actualCountry);
    }

    @Test
    @Ignore
    public void removeTest() {
	countryDao.remove(expectedContry);
	Assert.assertNull(countryDao.findById(expectedContry.getId()));
    }
}
