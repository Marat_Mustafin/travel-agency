/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 *
 * @author Marat_Mustafin
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class TourSearchDto {

    @NonNull
    private float[] cost;
    @NonNull
    private int[] duration;
    @NonNull
    private int[] year;
    @NonNull
    private String[] month;
    @NonNull
    private int[] day;
    @NotNull
    private int stars;
    @NotBlank
    @NotNull
    private String country;
    @NotBlank
    @NotNull
    private String tourType;
    @NotBlank
    @NotNull
    private String direction = "next";
    @NotNull
    private int limit;
    @NotNull
    private int[] id = new int[]{0, 0};
}
