/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.config;

import com.epam.travel_agency.dao.TourDao;
import com.epam.travel_agency.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;

/**
 *
 * @author Marat_Mustafin
 */
@Configuration
@ComponentScan(basePackages = "com.epam.travel_agency", excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = ServiceConfig.class)})
@Import(DaoConfig.class)
public class ServiceConfig {

}
