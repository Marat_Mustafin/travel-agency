package com.epam.travel_agency.service;

import com.epam.travel_agency.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.travel_agency.domain.Review;
import org.springframework.stereotype.Service;

@Service
public class ReviewService extends AbstractService<Review> {

    private static final String REVIEW_DAO = "reviewDao";

    public ReviewService(@Qualifier(REVIEW_DAO) AbstractDao<Review> reviewDao) {
	super(reviewDao);
    }

}
