package com.epam.travel_agency.service;

import com.epam.travel_agency.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.travel_agency.domain.Country;
import org.springframework.stereotype.Service;

@Service
public class CountryService extends AbstractService<Country> {

    private static final String COUNTRY_DAO = "countryDao";

    @Autowired
    public CountryService(@Qualifier(COUNTRY_DAO) AbstractDao<Country> countryDao) {
	super(countryDao);
    }

}
