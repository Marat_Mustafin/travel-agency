package com.epam.travel_agency.service;

import com.epam.travel_agency.dao.AbstractDao;
import com.epam.travel_agency.dao.specification.tour.AllToursByUserSpecification;
import com.epam.travel_agency.dao.specification.tour.SearchFormSpecification;
import com.epam.travel_agency.dao.specification.tour.DiapasonCostsListSpecification;
import com.epam.travel_agency.dao.specification.tour.DiapasonDateListSpecification;
import com.epam.travel_agency.dao.specification.tour.DiapasonDurationListSpecification;
import com.epam.travel_agency.dao.specification.tour.NextTourPaginationSpecification;
import com.epam.travel_agency.dao.specification.tour.PreviousTourPaginationSpecification;
import com.epam.travel_agency.domain.Country;
import com.epam.travel_agency.domain.Tour;
import com.epam.travel_agency.domain.User;
import com.epam.travel_agency.dto.TourSearchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class TourService extends AbstractService<Tour> {

    private static final String TOUR_DAO = "tourDao";
    private static final String AVAILABLE_TOUR_TYPES = "availableTourTypes";
    private static final String DATE_DIAPASON_LOCAL_DATE = "dateDiapasonLocalDate";
    private static final String DURATION_DIAPASON_INT = "durationDiapasonInt";
    private static final String COST_DIAPASON_FLOAT = "costDiapasonFloat";
    private static final String NEXT = "next";

    @Autowired
    private NextTourPaginationSpecification findNextTourPagination;

    @Autowired
    private PreviousTourPaginationSpecification findPreviousTourPagination;

    @Autowired
    private DiapasonCostsListSpecification diapasonCostsListSpecification;

    @Autowired
    private DiapasonDurationListSpecification diapasonDurationListSpecification;

    @Autowired
    private DiapasonDateListSpecification diapasonDateListSpecification;

    @Autowired
    private AllToursByUserSpecification allToursByUserSpecification;

    @Autowired
    public TourService(@Qualifier(TOUR_DAO) AbstractDao<Tour> tourDao) {
	super(tourDao);
    }

    public List<Tour> paginationFindNext(int id, int limit) {
	findNextTourPagination.setId(id);
	findNextTourPagination.setLimit(limit);
	return dao.findBySpecification(findNextTourPagination);
    }

    public List<Tour> paginationFindPrevious(int id, int limit) {
	findPreviousTourPagination.setId(id);
	findPreviousTourPagination.setLimit(limit);
	return dao.findBySpecification(findPreviousTourPagination);
    }

    public Map<String, String[]> findTourSearchFormParts() {
	Map<String, String[]> tourSearchFormParts = new HashMap<>();

	List<Float> costList = (List<Float>) dao.findObjectBySpecification(diapasonCostsListSpecification);
	String[] costArray = new String[2];
	costArray[0] = Float.toString(costList.get(0));
	costArray[1] = Float.toString(costList.get(1));
	tourSearchFormParts.put(COST_DIAPASON_FLOAT, costArray);

	List<Integer> durationList = (List<Integer>) dao.findObjectBySpecification(diapasonDurationListSpecification);
	String[] durationArray = new String[2];
	durationArray[0] = Integer.toString(durationList.get(0));
	durationArray[1] = Integer.toString(durationList.get(1));
	tourSearchFormParts.put(DURATION_DIAPASON_INT, durationArray);

	List<LocalDate> dateList = (List<LocalDate>) dao.findObjectBySpecification(diapasonDateListSpecification);
	String[] dateArray = new String[2];
	dateArray[0] = dateList.get(0).format(DateTimeFormatter.ISO_DATE);
	dateArray[1] = dateList.get(1).format(DateTimeFormatter.ISO_DATE);
	tourSearchFormParts.put(DATE_DIAPASON_LOCAL_DATE, dateArray);

	Tour.TourType[] tourTypes = Tour.TourType.values();
	String[] tourTypeName = new String[tourTypes.length];
	for (int i = 0; i < tourTypeName.length; i++) {
	    tourTypeName[i] = tourTypes[i].toString();
	}
	tourSearchFormParts.put(AVAILABLE_TOUR_TYPES, tourTypeName);
	return tourSearchFormParts;
    }

    @Autowired
    private SearchFormSpecification searchFormSpecification;

    public List<Tour> findBySearchForm(TourSearchDto tourSearchDto) {
	searchFormSpecification.setDirection(SearchFormSpecification.Direction.valueOf(tourSearchDto.getDirection().toUpperCase()));
	searchFormSpecification.setLimit(tourSearchDto.getLimit());
	searchFormSpecification.setCost(tourSearchDto.getCost());
	searchFormSpecification.setStars(tourSearchDto.getStars());
	searchFormSpecification.setTourType(Tour.TourType.valueOf(tourSearchDto.getTourType().toUpperCase()));
	searchFormSpecification.setDuration(tourSearchDto.getDuration());
	searchFormSpecification.setCountry(new Country(tourSearchDto.getCountry()));
	String allMonths[] = Arrays.copyOf(new DateFormatSymbols().getMonths(), 12);
	String currentMonths[] = tourSearchDto.getMonth();
	LocalDate date[] = new LocalDate[2];
	for (int k = 0; k < currentMonths.length; k++) {
	    for (int i = 0; i < allMonths.length; i++) {
		if (currentMonths[k].equals(allMonths[i])) {
		    date[k] = LocalDate.of(tourSearchDto.getYear()[k], Month.of(i + 1), tourSearchDto.getDay()[k]);
		}
	    }
	}
	searchFormSpecification.setDate(date);
	searchFormSpecification.setId((tourSearchDto.getDirection().equals(NEXT)) ? tourSearchDto.getId()[1] : tourSearchDto.getId()[0]);
	return dao.findBySpecification(searchFormSpecification);
    }

    public List<Tour> findByUser(User user) {
	allToursByUserSpecification.setUser(user);
	return dao.findBySpecification(allToursByUserSpecification);
    }
}
