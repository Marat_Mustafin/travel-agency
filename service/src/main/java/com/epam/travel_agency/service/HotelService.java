package com.epam.travel_agency.service;

import com.epam.travel_agency.dao.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.epam.travel_agency.domain.Hotel;
import org.springframework.stereotype.Service;

@Service
public class HotelService extends AbstractService<Hotel> {

    private static final String HOTEL_DAO = "hotelDao";

    @Autowired
    public HotelService(@Qualifier(HOTEL_DAO) AbstractDao<Hotel> hotelDao) {
	super(hotelDao);
    }

}
