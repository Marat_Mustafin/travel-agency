package com.epam.travel_agency.service;

import com.epam.travel_agency.dao.specification.user.ByLoginSpecification;
import com.epam.travel_agency.dao.AbstractDao;
import com.epam.travel_agency.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class UserService extends AbstractService<User> {

    private static final String USER_DAO = "userDao";
    
    @Autowired
    private ByLoginSpecification findUserByLoginSpecification;

    @Autowired
    public UserService(@Qualifier(USER_DAO) AbstractDao<User> userDao) {
	super(userDao);
    }

    public List<User> findByLogin(String login) {
	findUserByLoginSpecification.setLogin(login);
	return dao.findBySpecification(findUserByLoginSpecification);
    }

    public List<User> findAndReviews(String login) {
	findUserByLoginSpecification.setLogin(login);
	findUserByLoginSpecification.setLoadReview(true);
	return dao.findBySpecification(findUserByLoginSpecification);
    }

}
