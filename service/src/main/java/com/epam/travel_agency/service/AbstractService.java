package com.epam.travel_agency.service;

import com.epam.travel_agency.dao.AbstractDao;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public abstract class AbstractService<T> {

    protected AbstractDao<T> dao;

    private final Logger LOGGER = LoggerFactory.getLogger(AbstractService.class);

    @Autowired
    public AbstractService(AbstractDao<T> abstractDao) {
	this.dao = abstractDao;
    }

    public List<T> findAll() {
	LOGGER.debug("Request to find all entities of specified type");
	return dao.findAll();
    }

    public T findById(int id) {
	LOGGER.debug("Request to find entity by id of specified type", id);
	return dao.findById(id).get();
    }

    public void add(T t) {
	LOGGER.debug("Request to create entity", t.getClass());
	dao.add(t);
    }

    public void remove(T t) {
	LOGGER.debug("Request to remove entity", t.getClass());
	dao.remove(t);
    }

    public void update(T t) {
	LOGGER.debug("Request to update entity", t.getClass());
	dao.update(t);
    }
}
