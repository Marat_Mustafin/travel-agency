/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency;

import com.epam.travel_agency.dao.CountryDao;
import com.epam.travel_agency.domain.Country;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Marat_Mustafin
 */
@RunWith(MockitoJUnitRunner.class)
public class CountryServiceTest {

    @Mock
    private CountryDao mCountryDao;

    @InjectMocks
    private CountryService service = new CountryService(mCountryDao);

    @Test
    public void addTest() {
	service.add(new Country());
	verify(mCountryDao, only()).add(any(Country.class));
    }

    @Test
    public void findByIdTest() {
	Country expected = new Country();
	expected.setId(1);
	when(mCountryDao.findById(1)).thenReturn(expected);
	Country actual = service.findById(expected.getId()).get();
	Assert.assertEquals(Optional.of(expected).get(), actual);
	verify(mCountryDao, only()).findById(1);
    }

    @Test
    public void updateTest() {
	Country expected = new Country();
	expected.setId(1);
	service.update(expected);
	verify(mCountryDao, only()).update(expected);
    }

    @Test
    public void deleteTest() {
	Country expected = new Country();
	expected.setId(1);
	service.remove(expected);
	verify(mCountryDao, only()).remove(expected);
    }
}
