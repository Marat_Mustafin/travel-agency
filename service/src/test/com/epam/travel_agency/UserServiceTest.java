/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.service;

import com.epam.travel_agency.dao.UserDao;
import com.epam.travel_agency.domain.User;
import com.epam.travel_agency.UserService;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Marat_Mustafin
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserDao mUserDao;

    @InjectMocks
    private UserService service = new UserService(mUserDao);

    @Test
    public void addTest() {
	service.add(new User());
	verify(mUserDao, only()).add(any(User.class));
    }

    @Test
    public void findByIdTest() {
	User expected = new User();
	expected.setId(1);
	when(mUserDao.findById(1)).thenReturn(expected);
	User actual = service.findById(expected.getId()).get();
	Assert.assertEquals(Optional.of(expected).get(), actual);
	verify(mUserDao, only()).findById(1);
    }

    @Test
    public void updateTest() {
	User expected = new User();
	expected.setId(1);
	service.update(expected);
	verify(mUserDao, only()).update(expected);
    }

    @Test
    public void deleteTest() {
	User expected = new User();
	expected.setId(1);
	service.remove(expected);
	verify(mUserDao, only()).remove(expected);
    }
}
