/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.service;

import com.epam.travel_agency.dao.ReviewDao;
import com.epam.travel_agency.domain.Review;
import com.epam.travel_agency.ReviewService;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Marat_Mustafin
 */
@RunWith(MockitoJUnitRunner.class)
public class ReviewServiceTest {

    @Mock
    private ReviewDao mReviewDao;

    @InjectMocks
    private ReviewService service = new ReviewService(mReviewDao);

    @Test
    public void addTest() {
	service.add(new Review());
	verify(mReviewDao, only()).add(any(Review.class));
    }

    @Test
    public void findByIdTest() {
	Review expected = new Review();
	expected.setId(1);
	when(mReviewDao.findById(1)).thenReturn(expected);
	Review actual = service.findById(expected.getId()).get();
	Assert.assertEquals(Optional.of(expected).get(), actual);
	verify(mReviewDao, only()).findById(1);
    }

    @Test
    public void updateTest() {
	Review expected = new Review();
	expected.setId(1);
	service.update(expected);
	verify(mReviewDao, only()).update(expected);
    }

    @Test
    public void deleteTest() {
	Review expected = new Review();
	expected.setId(1);
	service.remove(expected);
	verify(mReviewDao, only()).remove(expected);
    }
}
