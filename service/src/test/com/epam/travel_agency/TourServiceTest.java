/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.service;

import com.epam.travel_agency.dao.TourDao;
import com.epam.travel_agency.domain.Tour;
import com.epam.travel_agency.TourService;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Marat_Mustafin
 */
@RunWith(MockitoJUnitRunner.class)
public class TourServiceTest {

    @Mock
    private TourDao mTourDao;

    @InjectMocks
    private TourService service = new TourService(mTourDao);

    @Test
    public void addTest() {
	service.add(new Tour());
	verify(mTourDao, only()).add(any(Tour.class));
    }

    @Test
    public void findByIdTest() {
	Tour expected = new Tour();
	expected.setId(1);
	when(mTourDao.findById(1)).thenReturn(expected);
	Tour actual = service.findById(expected.getId()).get();
	Assert.assertEquals(Optional.of(expected).get(), actual);
	verify(mTourDao, only()).findById(1);
    }

    @Test
    public void updateTest() {
	Tour expected = new Tour();
	expected.setId(1);
	service.update(expected);
	verify(mTourDao, only()).update(expected);
    }

    @Test
    public void deleteTest() {
	Tour expected = new Tour();
	expected.setId(1);
	service.remove(expected);
	verify(mTourDao, only()).remove(expected);
    }
}
