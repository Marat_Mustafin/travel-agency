/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.service;

import com.epam.travel_agency.HotelService;
import com.epam.travel_agency.dao.HotelDao;
import com.epam.travel_agency.domain.Hotel;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Marat_Mustafin
 */
@RunWith(MockitoJUnitRunner.class)
public class HotelServiceTest {

    @Mock
    private HotelDao mHotelDao;

    @InjectMocks
    private HotelService service = new HotelService(mHotelDao);

    @Test
    public void addTest() {
	service.add(new Hotel());
	verify(mHotelDao, only()).add(any(Hotel.class));
    }

    @Test
    public void findByIdTest() {
	Hotel expected = new Hotel();
	expected.setId(1);
	when(mHotelDao.findById(1)).thenReturn(expected);
	Hotel actual = service.findById(expected.getId()).get();
	Assert.assertEquals(Optional.of(expected).get(), actual);
	verify(mHotelDao, only()).findById(1);
    }

    @Test
    public void updateTest() {
	Hotel expected = new Hotel();
	expected.setId(1);
	service.update(expected);
	verify(mHotelDao, only()).update(expected);
    }

    @Test
    public void deleteTest() {
	Hotel expected = new Hotel();
	expected.setId(1);
	service.remove(expected);
	verify(mHotelDao, only()).remove(expected);
    }
}
