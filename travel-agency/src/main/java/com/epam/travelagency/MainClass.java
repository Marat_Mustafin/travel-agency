/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travelagency;

import com.epam.travelagency.config.AppConfig;
import com.epam.travelagency.dao.TourDao;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Marat_Mustafin
 */
@EnableTransactionManagement
public class MainClass {

    static TourDao dao;

    public static void main(String[] args) {

	AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

	dao = context.getBean(TourDao.class);
	System.out.println(dao.findTourByHotelStars(5));
    }

}
