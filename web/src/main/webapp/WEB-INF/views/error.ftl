<#import "header-footer.ftl" as basic>
<@basic.header id="header" title="Error page"/>
<#if errorMsg??>
<h1>${errorMsg}</h1>
</#if>
<@basic.footer id="footer"/>
