<#import "header-footer.ftl" as basic>
<@basic.header id="header" title="Hotel page"/>
<table border='1'>
    <th>ID</th>
    <th>Name</th>
    <th>Stars</th>
    <th>Website</th>
    <th>Lalitude</th>
    <th>Longitude</th>
    <th>Features</th>
    <tr>
        <td>${hotel.getId()}</td>
        <td>${hotel.getName()}</td>
        <td>${hotel.getStars()}</td>
        <td>${hotel.getWebSite()}</td>
        <td>${hotel.getLalitude()}</td>
        <td>${hotel.getLongitude()}</td>
        <td>
            <#list hotel.features>
            <ul>
            <#items as feature>
                <li>
        ${feature.name()}
                    </li>
            </#items>
                </ul>
        <#else>
            <h2><font color="red">There is no available featires yet!</font></h2>
            </#list>
            </td>
        </tr>
    </table>
</br>
<#assign lalitude = hotel.getLalitude()>
<#assign longitude = hotel.getLongitude()>
<img src="https://static-maps.yandex.ru/1.x/?ll=${lalitude},${longitude}&z=12&l=map&pt=${lalitude},${longitude},round">
<@basic.footer id="footer"/>
