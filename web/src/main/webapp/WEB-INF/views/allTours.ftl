<#macro toursTable id toursCollection="" >
<#setting number_format="computer">
        <#list toursCollection>
<table border='1'>
    <th>ID</th>
    <th>Photo</th>
    <th>Country</th>
    <th>Hotel</th>
    <th>Date</th>
    <th>Duration</th>
    <th>Short description</th>
    <th>Cost</th>
    <th>Type of tour</th>
    <th>&nbsp;</th>
    <#items as item>
    <form action="tour" method="GET">
        <input type="hidden" name="id" value="${item.getId()}"/>
        <tr>
            <td>${item.getId()}</td>
            <td>${item.getPhotoName()}</td>
            <td>${item.getCountry().getName()}</td>
            <td>${item.getHotel().getName()}</td>
            <td>${item.getDate()}</td>
            <td>${item.getDuration()}</td>
            <td>${item.getDescription()}</td>
            <td>${item.getCost()}</td>
            <td>${item.getTourType()}</td>
            <td><input type="submit" value="Details"/></td>
            </tr>
        </form>
    </#items>
<#else>
    <h2>Sorry! There is no available tours now</h2>
</#list>
    </table>
<form action='${context}${formAction}' method='GET'>
    <i>Visible range of tours</i>&nbsp;
    <select size='1' name="limit">
        <option lablel='10' value="10" <#if limit == 10>selected</#if>>10</option>
        <option lablel='25' value="25" <#if limit == 25>selected</#if>>25</option>
        <option lablel='50' value="50" <#if limit == 50>selected</#if>>50</option>
        </select>
    </br>
    <button type='submit' name='direction' value='previous'>Previous</button>
    <button type='submit' name='direction' value='next'>Next</button>
    </form>
</#macro>