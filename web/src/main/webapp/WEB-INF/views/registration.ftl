<#import "header-footer.ftl" as basic>
<@basic.header id="header" title="Registration page"/>
<#if registrationStatus??>
<h2>${registrationStatus}</h2>
</#if>
<form name='f' action='${context}registration' method='POST' modelAttribute='user'>
    <input name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">
    <input name='username' type='text' placeholder='Login' required autofocus value='<#if usernameValue??>${usernameValue}</#if>'>
    <input name='password' type='password' placeholder='Password' required value='<#if passwordValue??>${passwordValue}</#if>'>
    <input name="submit" type="submit" value="Registration"/>
    </form>
<@basic.footer id="footer"/>