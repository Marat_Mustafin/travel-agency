<#macro header id title="" >

<#assign sec=JspTaglibs["http://www.springframework.org/security/tags"] />
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"] />
<#assign form=JspTaglibs["http://www.springframework.org/tags/form"] />
<#assign spring=JspTaglibs["http://www.springframework.org/tags"] />

<@c.set var="context" value=requestContext.getContextPath()+'/' scope="application" />

<html lang="en">
    <head>
        <title>${title}</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </head>
    <body>
        <header>
            <img src="" alt=${title}>
            <h3>${title}</h3>
            <p>${.now?string('hh:mm')}</p>
            </header>
         <@sec.authorize access="! isAuthenticated()">
        <#if !loginPage??><a href="${context}login">Sign in</a>&nbsp;</#if>
        <#if !registrationPage??><a href="${context}registration">Register</a></#if>
        </@sec.authorize>
        <@sec.authorize access="isAuthenticated()">
        <#if !userPage??>&nbsp;<a href="${context}user">Profile <@sec.authentication property="principal.username" /></a></#if>
        &nbsp;<a href="${context}logout">Logout</a>
        </@sec.authorize>
        </br>
        </br>
</#macro>

<#macro footer id>
        <footer>
            Copyright EPAM 2019
            </footer>    
        </body>
    </html>
</#macro>
