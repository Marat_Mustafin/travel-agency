<#import "header-footer.ftl" as basic>
<#import "allTours.ftl" as allToursPagination>
<#import "findTour.ftl" as findTourForm>
<@basic.header id="header" title="Home page"/>
<#if allCountries?? & tourParts??>
<@findTourForm.toursFind id="searchFormTour" coutriesList=allCountries tourPartsMap=tourParts/>
<#else>
<h2><font color="red">Something goes wrong. Sorry...</font></h2>
</#if>
</br>
 <#if tours??>
<@allToursPagination.toursTable id="toursTablePagination" toursCollection=tours />
<#else>
<h2>Sorry! There is no available tours now</h2>
<a href="${context}home"><--- Back</a>
   </#if>
<@basic.footer id="footer"/>
