<#macro calendar>
<td>
    <select name="day">
        <#list 1..31as day>
        <option value="${day}" >${day}</option>
        </#list>
        </select>
    </td>
<td>
    <select name="month">
        <#list months as month>
        <option value="${month}" >${month}</option>
        </#list>
        </select>
    </td>
<td>
    <select name="year">
        <#list 2014..2025 as year>
        <option value="${year}" >${year}</option>
        </#list>
        </select>
    </td>
</tr>
        </#macro>

<#macro toursFind id coutriesList="" tourPartsMap="">
<#setting number_format="computer">

<form action='${context}findTour' method="GET" modelAttribute='tour'>
    <#assign keys = tourPartsMap?keys>

<!-- tourParts -->
    <#list keys as item>

<!-- tour types -->
        <#if item=="availableTourTypes">
    <h2><font color="green">${item}</font></h2>
            <#list tourPartsMap[item] as values>
    <input type="radio" name="tourType" value="${values}" <#if values=="CRUIS">checked</#if>>${values}&nbsp;
            </#list>
        </#if>

<!-- duration diapason -->
        <#if item=="durationDiapasonInt">
    <h2><font color="green">${item}</font></h2>
            <#list tourPartsMap[item] as values>
    <input name="duration" type="text" value="" placeholder="${values}">
            </#list>
        </#if>

<!-- date diapason -->
        <#if item=="dateDiapasonLocalDate">
    <h2><font color="green">${item}</font></h2>
            <#list tourPartsMap[item] as values>
    <li>${values}</li>
            </#list>
    From:&nbsp;<@calendar/>
    </br>
    To:&nbsp;<@calendar/>
        </#if>

<!-- cost diapason -->
        <#if item=="costDiapasonFloat">
    <h2><font color="green">${item}</font></h2>
            <#list tourPartsMap[item] as values>
    <input name="cost" type="text" value="" placeholder="${values}">
            </#list>
        </#if>

    </#list>

<!-- countries list -->
    <h2><font color="green">allCountries</font></h2>
    <select name="country">
            <#list allCountries as values>
        <option value="${values.name}" >${values.name}</option>
            </#list>
        </select>

<!-- hotel stars -->
    </br>
    <h2><font color="green">Hotel stars</font></h2>
    <#list 1..5 as star>
    <input name="stars" type="radio" value="${star}" <#if 3==star>checked</#if>>${star}&nbsp;
        </#list>
    </br>
    </br>
    </br>
    <input type='submit' value='Search'/>
    <input type='reset' value='Reset'/>
    </form>
</#macro>