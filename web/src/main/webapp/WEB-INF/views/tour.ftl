<#import "header-footer.ftl" as basic>
<@basic.header id="header" title="Tour page"/>
<table border='1'>
    <th>ID</th>
    <th>Country</th>
    <th>Hotel</th>
    <th>Date</th>
    <th>Duration</th>
    <th>Short description</th>
    <th>Cost</th>
    <th>Type of tour</th>
    <th>Country name</th>
    <th>Picture</th>
    <tr>
        <td>${tour.getId()}</td>
        <td>${tour.getCountry().getName()}</td>
        <td><a href='${context}hotel?id=${tour.getHotel().getId()}'>${tour.getHotel().getName()}</a></td>
        <td>${tour.getDate()}</td>
        <td>${tour.getDuration()}</td>
        <td>${tour.getDescription()}</td>
        <td>${tour.getCost()}</td>
        <td>${tour.getTourType()}</td>
        <td>${tour.getCountry().getName()}</td>
        <td><img src="images/${tour.getPhotoName()}"></td>
        </tr>
    </table>
        <#list tour.review>
<h2><font color="yellow">Reviews:</font></h2>
<ul>
            <#items as item>
    <li>
        ${item.getText()}
        </li>
            </#items>
    </ul>
        <#else>
<h2><font color="red">There is no available reviews yet!</font></h2>
            </#list>
<@basic.footer id="footer"/>
