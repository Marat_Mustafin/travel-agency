<#import "header-footer.ftl" as basic>
<@basic.header id="header" title="Login page"/>
<#if registrationStatus??>
<h2>${registrationStatus}</h2>
</#if>
<form name='f' action='${context}login' method='POST'>
    <input name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">
    <input name='username' type='text' placeholder='Login' required autofocus value='user'>
    <input name='password' type='password' placeholder='Password' required value='user'>
    <input name="submit" type="submit" value="Login"/>
    </form>
<@basic.footer id="footer"/>
