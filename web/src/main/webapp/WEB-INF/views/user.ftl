<#import "header-footer.ftl" as basic>
<#import "admin-member.ftl" as userProfile>
<#setting number_format="computer">
<@basic.header id="header" title="User page"/>
<@userProfile.member id="userData" userEntity=user tourEntity=tour/>
<#if admin??>
<@userProfile.admin id="adminPerform"/>
</#if>
<@basic.footer id="footer"/>
