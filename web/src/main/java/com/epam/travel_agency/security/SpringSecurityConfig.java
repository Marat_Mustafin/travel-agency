/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 *
 * @author Marat_Mustafin
 */
@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.epam.travel_agency", excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = SpringSecurityConfig.class)})
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String LOGOUT = "/logout";
    private static final String LOGIN = "/login";
    private static final String FIND_TOUR = "/findTour";
    private static final String TOUR = "/tour";
    private static final String HOME = "/home";
    private static final String ERROR = "/error";
    private static final String REGISTRATION = "/registration";
    private static final String INDEX = "/index";
    private static final String STRING = "/";

    @Bean
    public UserDetailsService detailsService() {
	return new UserDetailsServiceImpl();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
	return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
	DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
	authProvider.setUserDetailsService(detailsService());
	authProvider.setPasswordEncoder(passwordEncoder());
	return authProvider;
    }

    @Autowired
    @Override
    public void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
	authManagerBuilder.authenticationProvider(authProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
	http
		.authorizeRequests()
		.antMatchers(STRING, INDEX, REGISTRATION, ERROR, HOME, TOUR, FIND_TOUR).permitAll()
		.anyRequest().authenticated()
		.and()
		.formLogin().permitAll()
		.loginPage(LOGIN)
		.defaultSuccessUrl(HOME)
		.and()
		.logout().logoutRequestMatcher(new AntPathRequestMatcher(LOGOUT))
		.invalidateHttpSession(true)
		.permitAll();
    }

}
