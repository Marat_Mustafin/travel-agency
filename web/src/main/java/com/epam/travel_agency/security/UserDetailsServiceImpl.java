/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.security;

import com.epam.travel_agency.domain.User;
import com.epam.travel_agency.service.UserService;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 *
 * @author Marat_Mustafin
 */
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
	List<User> userList = userService.findByLogin(string);
	if (userList.isEmpty()) {
	    throw new UsernameNotFoundException(string);
	}
	User user = userList.get(0);
	Set<GrantedAuthority> roles = new HashSet();
	roles.add(new SimpleGrantedAuthority(user.getUserRole().name()));
	UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), roles);
	return userDetails;
    }
}
