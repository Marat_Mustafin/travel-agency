/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.controller;

import com.epam.travel_agency.domain.Tour;
import com.epam.travel_agency.domain.User;
import com.epam.travel_agency.dto.TourSearchDto;
import com.epam.travel_agency.service.CountryService;
import com.epam.travel_agency.service.HotelService;
import com.epam.travel_agency.service.TourService;
import com.epam.travel_agency.service.UserService;
import com.epam.travel_agency.userrole.UserRoleEnum;
import java.security.Principal;
import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Marat_Mustafin
 */
@Controller
@RequestMapping("/")
public class AppController {

    private static final String INDEX = "index";
    private static final String LOGIN = "login";
    private static final String REGISTRATION = "regiistration";
    private static final String HOME = "home";
    private static final String ERROR = "error";
    private static final String TOUR = "tour";
    private static final String USER = "user";
    private static final String HOTEL = "hotel";
    private static final String LOGIN_PAGE = "loginPage";
    private static final String REGISTRATIOIN_PAGE = "registrationPage";
    private static final String TOUR_PARTS = "tourParts";
    private static final String PASSWORD_VALUE = "passwordValue";
    private static final String USERNAME_VALUE = "usernameValue";
    private static final String REGISTRATION_STATUS = "registrationStatus";
    private static final String NUMBER_10 = "10";
    private static final String NONE = "NONE0";
    private static final String FORM_ACTION = "formAction";
    private static final String MONTHS = "months";
    private static final String ALL_COUNTRIES = "allCountries";
    private static final String ERROR_MSG = "errorMsg";
    private static final String TOUR_DTO = "tourDto";
    private static final String ADMIN = "admin";
    private static final String USER_PAGE = "userPage";
    private static final String FIND_TOUR = "findTour";
    private static final String LIMIT = "limit";
    private static final String PREVIOUS = "previous";
    private static final String ID = "id";
    private static final String TOURS = "tours";
    private static final String NEXT = "next";
    private static final String JAVAXSERVLETERRORSTATUS_CODE = "javax.servlet.error.status_code";

    @GetMapping("/")
    public String index() {
	return INDEX;
    }

    @GetMapping("/" + LOGIN)
    public String showLoginForm(Model model) {
	model.addAttribute(LOGIN_PAGE, true);
	return LOGIN;
    }

    @PostMapping("/" + LOGIN)
    public String processLoginForm() {
	return LOGIN;
    }

    @GetMapping("/" + REGISTRATION)
    public String showRegistrationForm(Model model) {
	model.addAttribute(REGISTRATIOIN_PAGE, true);
	return REGISTRATION;
    }

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/" + REGISTRATION)
    public String processRegistrationForm(
	    Model model, @ModelAttribute(USER) final User user,
	    @RequestParam(required = true) String username,
	    @RequestParam(required = true) String password) {
	if (userService.findByLogin(username).isEmpty()) {
	    user.setUserRole(UserRoleEnum.MEMBER);
	    user.setPassword(passwordEncoder.encode(user.getPassword()));
	    userService.add(user);
	    model.addAttribute(REGISTRATION_STATUS, "You are successfully registered");
	    return LOGIN;
	} else {
	    model.addAttribute(REGISTRATION_STATUS, "Username \"" + username + "\" alredy exits");
	    model.addAttribute(USERNAME_VALUE, user.getUsername());
	    model.addAttribute(PASSWORD_VALUE, user.getPassword());
	    return REGISTRATION;
	}
    }

    @Autowired
    private TourService tourService;

    @Autowired
    private CountryService countryService;

    @GetMapping("/" + HOME)
    public String showHomePage(final Model model,
	    @RequestParam(defaultValue = NUMBER_10) Integer limit,
	    @RequestParam(defaultValue = NONE) String direction,
	    HttpSession session) {
	allToursPagination(model, limit, direction, session);
	model.addAttribute(ALL_COUNTRIES, countryService.findAll());
	model.addAttribute(TOUR_PARTS, tourService.findTourSearchFormParts());
	model.addAttribute(MONTHS, Arrays.copyOf(new DateFormatSymbols().getMonths(), 12));
	model.addAttribute(FORM_ACTION, HOME);
	return HOME;
    }

    private void allToursPagination(Model model, Integer limit, String direction, HttpSession session) {
	List<Tour> tours = null;
	int[] id = (null != session.getAttribute(ID)) ? (int[]) session.getAttribute(ID) : new int[]{0, 0};
	if (direction.equals(NEXT)) {
	    tours = tourService.paginationFindNext(id[1], limit);
	    if (!tours.isEmpty()) {
		model.addAttribute(TOURS, tours);
		id = new int[]{tours.get(0).getId(), tours.get(tours.size() - 1).getId()};
	    }
	} else if (direction.equals(PREVIOUS)) {
	    tours = tourService.paginationFindPrevious(id[0], limit);
	    if (!tours.isEmpty()) {
		Collections.reverse(tours);
		model.addAttribute(TOURS, tours);
		id = new int[]{tours.get(0).getId(), tours.get(tours.size() - 1).getId()};
	    }
	} else {
	    tours = tourService.paginationFindNext(0, limit);
	    model.addAttribute(TOURS, tours);
	    id = new int[]{tours.get(0).getId(), tours.get(tours.size() - 1).getId()};
	}
	session.setAttribute(ID, id);
	model.addAttribute(LIMIT, limit);
    }

    @GetMapping("/" + ERROR)
    public String showErrorPage(HttpServletRequest httpRequest, Model model) {
	switch ((int) httpRequest.getAttribute(JAVAXSERVLETERRORSTATUS_CODE)) {
	    case 400: {
		model.addAttribute(ERROR_MSG, "Http Error Code: 400. Bad Request");
		break;
	    }
	    case 401: {
		model.addAttribute(ERROR_MSG, "Http Error Code: 401. Unauthorized");
		break;
	    }
	    case 403: {
		model.addAttribute(ERROR_MSG, "Http Error Code: 403. Forbidden");
		break;
	    }
	    case 405: {
		model.addAttribute(ERROR_MSG, "Http Error Code: 403. Method Not Allowed");
		break;
	    }
	    case 404: {
		model.addAttribute(ERROR_MSG, "Http Error Code: 404. Resource not found");
		break;
	    }
	    case 500: {
		model.addAttribute(ERROR_MSG, "Http Error Code: 500. Internal Server Error");
		break;
	    }
	}
	return ERROR;
    }

    @GetMapping("/" + TOUR)
    public String tour(Model model, @RequestParam(required = true) Integer id) {
	Tour tour = tourService.findById(id);
	model.addAttribute(TOUR, tour);
	return TOUR;
    }

    @GetMapping("/" + FIND_TOUR)
    public String findTour(final Model model,
	    @ModelAttribute(TOUR) TourSearchDto tourDto,
	    HttpSession session,
	    @RequestParam(defaultValue = NUMBER_10) Integer limit,
	    @RequestParam(defaultValue = NEXT) String direction) {

	if (tourDto.getTourType() != null) {
	    tourDto.setLimit(limit);
	    tourDto.setDirection(direction);
	    session.setAttribute(TOUR_DTO, tourDto);
	} else {
	    TourSearchDto tourDtoFromSession = (TourSearchDto) session.getAttribute(TOUR_DTO);
	    tourDtoFromSession.setLimit(limit);
	    tourDtoFromSession.setDirection(direction);
	    session.setAttribute(TOUR_DTO, tourDtoFromSession);
	}
	List<Tour> tour = tourService.findBySearchForm((TourSearchDto) session.getAttribute(TOUR_DTO));
	TourSearchDto tourDtoFromSession = (TourSearchDto) session.getAttribute(TOUR_DTO);
	if (!tour.isEmpty()) {
	    tourDtoFromSession.setId(new int[]{tour.get(0).getId(), tour.get(tour.size() - 1).getId()});
	    model.addAttribute(TOURS, tour);
	}
	session.setAttribute(TOUR_DTO, tourDtoFromSession);
	model.addAttribute(ALL_COUNTRIES, countryService.findAll());
	model.addAttribute(TOUR_PARTS, tourService.findTourSearchFormParts());
	model.addAttribute(MONTHS, Arrays.copyOf(new DateFormatSymbols().getMonths(), 12));
	model.addAttribute(LIMIT, limit);
	model.addAttribute(FORM_ACTION, FIND_TOUR);
	return HOME;
    }

    @Autowired
    private HotelService hotelService;

    @GetMapping("/" + HOTEL)
    public String hotel(Model model, @RequestParam(required = true) Integer id) {
	model.addAttribute(HOTEL, hotelService.findById(id));
	return HOTEL;
    }

    @GetMapping("/" + USER)
    public String user(Principal principal, Model model) {
	model.addAttribute(USER_PAGE, true);
	List<User> foundUser = userService.findAndReviews(principal.getName());
	User user = foundUser.get(0);
	model.addAttribute(USER, user);
	model.addAttribute(TOUR, tourService.findByUser(user));
	if (user.getUserRole() == UserRoleEnum.ADMIN) {
	    model.addAttribute(ADMIN, true);
	}
	return USER;
    }

}
