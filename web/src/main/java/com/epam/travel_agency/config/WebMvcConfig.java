/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.travel_agency.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

/**
 *
 * @author Marat_Mustafin
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.epam.travel_agency", excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = WebMvcConfig.class),})
public class WebMvcConfig implements WebMvcConfigurer {

    private final String SUFFIX_FTL = ".ftl";
    private final String PREFIX = "";
    private final String REQUEST_CONTEXT = "requestContext";
    private final String ROOT = "/";
    private final String WEB_INF_VIEWS = "/WEB-INF/views/";
    private final String IMAGES_RESOURSE = "/images/**";
    private final String IMAGES_LOCATION = "/images/";

    @Bean
    public ViewResolver createViewResolver() {
	FreeMarkerViewResolver freeMarkerViewResolver = new FreeMarkerViewResolver();
	freeMarkerViewResolver.setOrder(1);
	freeMarkerViewResolver.setCache(true);
	freeMarkerViewResolver.setSuffix(SUFFIX_FTL);
	freeMarkerViewResolver.setPrefix(PREFIX);
	freeMarkerViewResolver.setRequestContextAttribute(REQUEST_CONTEXT);
	return freeMarkerViewResolver;
    }

    @Bean
    public FreeMarkerConfigurer createFreeMarkerConfigurer() {
	FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
	freeMarkerConfigurer.setTemplateLoaderPaths(ROOT, WEB_INF_VIEWS);
	return freeMarkerConfigurer;
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
	configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
	registry
		.addResourceHandler(IMAGES_RESOURSE)
		.addResourceLocations(IMAGES_LOCATION);
    }

}
