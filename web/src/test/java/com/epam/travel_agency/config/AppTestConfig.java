package com.epam.travel_agency.config;

import junit.framework.TestCase;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "com.epam.travelagency", excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = AppTestConfig.class)})
public class AppTestConfig extends TestCase {

}
